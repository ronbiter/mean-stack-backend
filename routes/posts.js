const express = require('express');

const checkAuth = require('../middleware/check-auth');
const extractFile = require('../middleware/file');
const PostsController = require('../controllers/posts');

const router = express.Router();



router.post( '', checkAuth, extractFile, PostsController.createPost);

router.put('/:id', checkAuth, extractFile, PostsController.updatePost);

router.get('', PostsController.fetchAllPosts);

router.get('/:id', PostsController.fetchPostById);

router.delete('/:id', checkAuth, PostsController.deletePostById);

module.exports = router;